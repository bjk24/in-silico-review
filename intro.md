(page:intro)=
# Introduction

This Jupyter Book is a companion to the tutorial-review article
["In Silico Characterization of Nanoparticle Catalysts"](https://doi.org/10.1039/D3CP01073B).

This book is intended for newcomers to the field of computational
science to learn about data evaluation with python3 in particular.
Here, we will be using the popular `pandas` library to handle an
extensive data set of *ca.* 90,000 entries, implement various
analysis techniques with python code, and use `matplotlib` to
visualize the data. Finally, we will make use of the [Atomic
Simulation Environment (ASE)](https://wiki.fysik.dtu.dk/ase/)
python package, which is popular
in computational chemistry and physics, to facilitate working with
chemical model systems.

Note that the explanations in this book build on each other in
the order of consecutive pages. There will always be links to
other pages when we cut short explanations due to repetition.
However, if you are completely new to scientific data analysis
with python3, it is recommended to work through these pages
in the sequence of the table of contents to the left.

If you find an error, feel free to open an
[issue](https://gitlab.com/bjk24/in-silico-review/-/issues)!

```{note} A note on performance: the python3 code shown in this
book is implemented for didactic purposes with readability in mind.
The code is not highly optimized and any cryptic-seeming
vectorization using numpy arrays is mostly avoided, unless it
happens "under the hood" in some of the common functions that are
being used. Veterans will surely find ways to improve the speed
of the code! For production calculations, it will in most
circumstances be advisable to look for published software that
implements the presented methods in highly optimized (but often
also closed-source) fashion.
```

If you wish to execute the code in this book yourself, some
setup is needed. The necessary steps are outlined in the
[setup](page:setup) section.