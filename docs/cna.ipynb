{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(page:cna)=\n",
    "# Common Neighbor Analysis\n",
    "\n",
    "This script performs a Common Neighbor Analysis. CNA was originally introduced by Honeycut and Andersen ([J. Phys. Chem., 1987, 91, 4950–4963](https://pubs.acs.org/doi/pdf/10.1021/j100303a014)). Here, we use the triples definition implemented by Clarke and Jónsson ([Phys. Rev. E, 1993, 47, 3975–3984](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.47.3975)). A first draft of this code was contributed by Julian Bessner (Ulm University)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual, we start by reading in the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "data = pd.read_csv(\"../data/data.csv\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Select a non-oxidized system with 0 oxygen atoms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from asetools.systemtools import read_bgf\n",
    "\n",
    "sample = data.loc[(data[\"n_O\"] == 0)].iloc[0]\n",
    "atoms = read_bgf(sample[\"root\"])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to the [classical coordination number analysis](page:cns) and the [generalized coordination number analysis](page:gcns), this method analyzes the coordination environment of atoms. A cutoff parameter is required to determine if atoms are neighbors or not. Here, we use the same cutoff value $r_c$ that served us well in the other implementations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "rc = 3.0"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start off, we require a neighbor list. We will reuse the implementation from the [generalized coordination number analysis](page:gcns). We could also implement the `probe` parameter here to filter the indices but since CNA is mostly used on [single-element systems](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.47.3975), we leave it out this time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "neighborList = {}\n",
    "indices = [atom.index for atom in atoms]\n",
    "for idx in indices:\n",
    "\n",
    "    # Calculate all distances.\n",
    "    dists = atoms.get_distances(idx, indices)\n",
    "\n",
    "    # Initialize empty list for current atom index.\n",
    "    neighborList.update({idx: []})\n",
    "\n",
    "    # Add indices of neighbor atoms to the list.\n",
    "    for i, d in enumerate(dists):\n",
    "        if d > 0 and d <= rc:\n",
    "            neighborList[idx].append(indices[i])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now start building the list of $jkl$ triples. The $jkl$ indices stand for:\n",
    "\n",
    "* $j$: Number of common neighbors of two atoms.\n",
    "* $k$: Number of bonds between these common neighbors.\n",
    "* $l$: Longest chain of connected common neighbors.\n",
    "\n",
    "The nested loops we are about to build are likely to become visually confusing, so we first section off code into two separate function to improve legibility of the code.\n",
    "\n",
    "The first function finds $k$, which is the number of bonds between common neighbors of the current pair of atoms. The common neighbors are supplied as a list via the `common` argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def findConnected(common, atoms, rc):\n",
    "    \"\"\"Determine number of bonds between common neighbors.\"\"\"\n",
    "    connected = []\n",
    "    for i, commonAtom1 in enumerate(common[:-1]):\n",
    "        # commonAtom are atomic indices which relate back to the\n",
    "        # original atoms list.\n",
    "        for commonAtom2 in common[i + 1:]:\n",
    "            dist = atoms.get_distance(commonAtom1, commonAtom2)\n",
    "            if dist <= rc:\n",
    "                # Adding tuples of (commonAtom1, commonAtom2)\n",
    "                # to connected list.\n",
    "                connected.append((commonAtom1, commonAtom2))\n",
    "    return connected"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `findConnected` function makes use of `ase.Atoms.get_distance` again which can be supplied with the `mic = True` keyword to make this function work properly for periodic systems such as crystals.\n",
    "\n",
    "The second function is used to find $l$, the longest connected chain of bonds within a given list of common neighbor atoms. As input it receives the output of the previous function, a list called `common`, which contains the number of bonds between common neighbors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from itertools import combinations\n",
    "\n",
    "def maxBondChain(connected):\n",
    "    \"\"\"Calculate the longest connected chain of connected bonds.\"\"\"\n",
    "    listMaxBondChain = []\n",
    "    for i, j in combinations(connected, 2):\n",
    "        if (i[0]  == j[0] or i[0] == j[1] or i[1] == j[0] or i[1] == j[1]):\n",
    "            listMaxBondChain.extend([i, j])\n",
    "        elif (i[0] != j[0] and i[0] != j[1] and i[1] != j[0]\n",
    "                and i[1] != j[1]):\n",
    "            listMaxBondChain.append(i)\n",
    "        else:\n",
    "            continue\n",
    "    return len(set(listMaxBondChain))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell performs the actual calculation of $jkl$ in a nested `for` loop. The first `for` loop selects each atom. The nested, inside `for` loop then iterates through its neighbor atoms. We explicitly calculate $j$ in the first section of the inner loop as the intersection between the `neighborLists` of `atom1` and `atom2`. For the calculation of $k$ and $l$, the functions defined above are called to reduce the visual clutter in this loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "jklList = {}\n",
    "for atom1, neighbors1 in neighborList.items():\n",
    "    for atom2 in neighbors1:\n",
    "\n",
    "        # Find intersection of neighbor lists. This gives us the j of\n",
    "        # the (jkl) triple.\n",
    "        neighbors2 = neighborList[atom2]\n",
    "        common = list(set(neighbors1) & set(neighbors2))\n",
    "        j = len(common)\n",
    "\n",
    "        # Next, find number of bonds between these common neighbors\n",
    "        # This gives us the k of the (jkl) triple.\n",
    "        connected = findConnected(common, atoms, rc)\n",
    "        k = len(connected)\n",
    "\n",
    "        # Find the longest chain of connected common neighbors of the pair.\n",
    "        # This gives us l of the (jkl) triple.\n",
    "        l = maxBondChain(connected)\n",
    "\n",
    "        # Insert to jkl dictionary.\n",
    "        jklList.update({atom1: (j, k, l)})"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`jklList` now contains an entry for each pair of neighboring atoms. We are, however, more interested in the distributions of $jkl$ for the entire system. Loop through `jklList` and count how often each $jkl$ triple occurs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "jklDist = {}\n",
    "for jkl in jklList.values():\n",
    "    if jkl != (0, 0, 0):\n",
    "        if jkl not in jklDist:\n",
    "            jklDist.update({jkl: 1})\n",
    "        else:\n",
    "            jklDist[jkl] += 1"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, visualize the distribution. Here, we overwrite the `xticks` list of the `pyplot` figure with the names of the $jkl$ triples to make those into the $x$ axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAArcAAAIPCAYAAABpBPZvAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8qNh9FAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAws0lEQVR4nO3de3BU9f3/8VcuJBBgNwZIlpRwEaGA3Cwo5CtekJgEkC8IziiCBMyI0MSqAbWxfomiNoqKigXSma8GmcoXyhS8gGAx4SI1gEbu1CgRCJobJSaBWJZczu+PDvtzJUCyLNnkw/Mxc2bYc86efZ+ZnubZ05ONn2VZlgAAAAAD+Pt6AAAAAMBbiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGCPQlx++dOlSLV26VEePHpUkXX/99Zo3b55Gjx4tSTpz5ozmzJmjlStXyul0Ki4uTkuWLFFERITrGAUFBZo9e7Y2b96sdu3aKSEhQenp6QoMbPip1dXVqbCwUO3bt5efn59XzxEAAACXz7IsnTp1SpGRkfL3v8j9WcuHPvzwQ2v9+vXWN998Y+Xl5VlPP/201apVK+vAgQOWZVnWrFmzrKioKCsrK8v68ssvreHDh1v/9V//5Xp/TU2N1b9/fysmJsbavXu39fHHH1sdO3a0UlNTGzXH8ePHLUksLCwsLCwsLCzNfDl+/PhFu87PsixLzUhYWJheeeUV3XPPPerUqZNWrFihe+65R5L09ddfq2/fvsrJydHw4cO1YcMG3XXXXSosLHTdzc3IyNBTTz2lEydOKCgoqEGfWVFRodDQUB0/flw2m+2KnRsAAAA8U1lZqaioKJWXl8tut19wP58+lvBztbW1Wr16taqqqhQdHa3c3FxVV1crJibGtU+fPn3UtWtXV9zm5ORowIABbo8pxMXFafbs2Tp48KBuuOGGej/L6XTK6XS6Xp86dUqSZLPZiFsAAIBm7FKPkPr8F8r279+vdu3aKTg4WLNmzdLatWvVr18/FRcXKygoSKGhoW77R0REqLi4WJJUXFzsFrbntp/bdiHp6emy2+2uJSoqyrsnBQAAAJ/wedz++te/1p49e7Rz507Nnj1bCQkJOnTo0BX9zNTUVFVUVLiW48ePX9HPAwAAQNPw+WMJQUFBuu666yRJQ4YM0RdffKE333xT9957r86ePavy8nK3u7clJSVyOBySJIfDoV27drkdr6SkxLXtQoKDgxUcHOzlMwEAAICv+fzO7S/V1dXJ6XRqyJAhatWqlbKyslzb8vLyVFBQoOjoaElSdHS09u/fr9LSUtc+mzZtks1mU79+/Zp8dgAAAPiWT+/cpqamavTo0eratatOnTqlFStWaMuWLfrkk09kt9uVmJiolJQUhYWFyWaz6ZFHHlF0dLSGDx8uSYqNjVW/fv30wAMPaMGCBSouLtYzzzyjpKQk7swCAABchXwat6WlpZo2bZqKiopkt9s1cOBAffLJJ7rzzjslSa+//rr8/f01adIktz/icE5AQIDWrVun2bNnKzo6Wm3btlVCQoLmz5/vq1MCAACADzW777n1hcrKStntdlVUVPBVYAAAAM1QQ3ut2T1zCwAAAHiKuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDECfT0AAABovO6/X+/rEXCVO/rSWF+PUC/u3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjOHTuE1PT9eNN96o9u3bKzw8XBMmTFBeXp7bPrfffrv8/PzcllmzZrntU1BQoLFjxyokJETh4eF64oknVFNT05SnAgAAgGYg0JcfvnXrViUlJenGG29UTU2Nnn76acXGxurQoUNq27ata7+HHnpI8+fPd70OCQlx/bu2tlZjx46Vw+HQ559/rqKiIk2bNk2tWrXSH//4xyY9HwAAAPiWT+N248aNbq+XLVum8PBw5ebm6tZbb3WtDwkJkcPhqPcYf//733Xo0CF9+umnioiI0ODBg/X888/rqaee0rPPPqugoKAreg4AAABoPprVM7cVFRWSpLCwMLf17733njp27Kj+/fsrNTVVP/30k2tbTk6OBgwYoIiICNe6uLg4VVZW6uDBg00zOAAAAJoFn965/bm6ujo99thjuvnmm9W/f3/X+vvvv1/dunVTZGSk9u3bp6eeekp5eXlas2aNJKm4uNgtbCW5XhcXF9f7WU6nU06n0/W6srLS26cDAAAAH2g2cZuUlKQDBw5o+/btbutnzpzp+veAAQPUuXNnjRo1Svn5+erZs6dHn5Wenq7nnnvusuYFAABA89MsHktITk7WunXrtHnzZnXp0uWi+w4bNkySdPjwYUmSw+FQSUmJ2z7nXl/oOd3U1FRVVFS4luPHj1/uKQAAAKAZ8GncWpal5ORkrV27VtnZ2erRo8cl37Nnzx5JUufOnSVJ0dHR2r9/v0pLS137bNq0STabTf369av3GMHBwbLZbG4LAAAAWj6fPpaQlJSkFStW6IMPPlD79u1dz8ja7Xa1adNG+fn5WrFihcaMGaMOHTpo3759evzxx3Xrrbdq4MCBkqTY2Fj169dPDzzwgBYsWKDi4mI988wzSkpKUnBwsC9PDwAAAE3Mp3duly5dqoqKCt1+++3q3Lmza1m1apUkKSgoSJ9++qliY2PVp08fzZkzR5MmTdJHH33kOkZAQIDWrVungIAARUdHa+rUqZo2bZrb9+ICAADg6uDTO7eWZV10e1RUlLZu3XrJ43Tr1k0ff/yxt8YCAABAC9UsfqEMAAAA8AbiFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxiBuAQAAYAziFgAAAMYgbgEAAGAM4hYAAADGIG4BAABgDOIWAAAAxvBp3Kanp+vGG29U+/btFR4ergkTJigvL89tnzNnzigpKUkdOnRQu3btNGnSJJWUlLjtU1BQoLFjxyokJETh4eF64oknVFNT05SnAgAAgGbAp3G7detWJSUlaceOHdq0aZOqq6sVGxurqqoq1z6PP/64PvroI61evVpbt25VYWGhJk6c6NpeW1ursWPH6uzZs/r888/17rvvatmyZZo3b54vTgkAAAA+5GdZluXrIc45ceKEwsPDtXXrVt16662qqKhQp06dtGLFCt1zzz2SpK+//lp9+/ZVTk6Ohg8frg0bNuiuu+5SYWGhIiIiJEkZGRl66qmndOLECQUFBV3ycysrK2W321VRUSGbzXZFzxEAAG/o/vv1vh4BV7mjL41t0s9raK81q2duKyoqJElhYWGSpNzcXFVXVysmJsa1T58+fdS1a1fl5ORIknJycjRgwABX2EpSXFycKisrdfDgwXo/x+l0qrKy0m0BAABAy9ds4raurk6PPfaYbr75ZvXv31+SVFxcrKCgIIWGhrrtGxERoeLiYtc+Pw/bc9vPbatPenq67Ha7a4mKivLy2QAAAMAXmk3cJiUl6cCBA1q5cuUV/6zU1FRVVFS4luPHj1/xzwQAAMCVF+jrASQpOTlZ69at07Zt29SlSxfXeofDobNnz6q8vNzt7m1JSYkcDodrn127drkd79y3KZzb55eCg4MVHBzs5bMAAACAr/n0zq1lWUpOTtbatWuVnZ2tHj16uG0fMmSIWrVqpaysLNe6vLw8FRQUKDo6WpIUHR2t/fv3q7S01LXPpk2bZLPZ1K9fv6Y5EQAAADQLPr1zm5SUpBUrVuiDDz5Q+/btXc/I2u12tWnTRna7XYmJiUpJSVFYWJhsNpseeeQRRUdHa/jw4ZKk2NhY9evXTw888IAWLFig4uJiPfPMM0pKSuLuLAAAwFXGp3G7dOlSSdLtt9/utj4zM1PTp0+XJL3++uvy9/fXpEmT5HQ6FRcXpyVLlrj2DQgI0Lp16zR79mxFR0erbdu2SkhI0Pz585vqNAAAANBMNKvvufUVvucWANDS8D238DW+5xYAAAC4wohbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxiFsAAAAYg7gFAACAMYhbAAAAGIO4BQAAgDGIWwAAABiDuAUAAIAxPIrb7777zttzAAAAAJfNo7i97rrrNHLkSP3lL3/RmTNnvD0TAAAA4BGP4varr77SwIEDlZKSIofDoYcffli7du3y9mwAAABAo3gUt4MHD9abb76pwsJCvfPOOyoqKtKIESPUv39/LVy4UCdOnPD2nAAAAMAlXdYvlAUGBmrixIlavXq1Xn75ZR0+fFhz585VVFSUpk2bpqKiIm/NCQAAAFzSZcXtl19+qd/+9rfq3LmzFi5cqLlz5yo/P1+bNm1SYWGhxo8f7605AQAAgEsK9ORNCxcuVGZmpvLy8jRmzBgtX75cY8aMkb//f1q5R48eWrZsmbp37+7NWQEAAICL8ihuly5dqgcffFDTp09X586d690nPDxcb7/99mUNBwAAADSGR3H77bffXnKfoKAgJSQkeHJ4AAAAwCMePXObmZmp1atXn7d+9erVevfddy97KAAAAMATHsVtenq6OnbseN768PBw/fGPf7zsoQAAAABPeBS3BQUF6tGjx3nru3XrpoKCgsseCgAAAPCER3EbHh6uffv2nbd+79696tChw2UPBQAAAHjCo7idPHmyfve732nz5s2qra1VbW2tsrOz9eijj+q+++7z9owAAABAg3j0bQnPP/+8jh49qlGjRikw8D+HqKur07Rp03jmFgAAAD7jUdwGBQVp1apVev7557V37161adNGAwYMULdu3bw9HwAAANBgHsXtOb1791bv3r29NQsAAABwWTyK29raWi1btkxZWVkqLS1VXV2d2/bs7GyvDAcAAAA0hkdx++ijj2rZsmUaO3as+vfvLz8/P2/PBQAAADSaR3G7cuVK/fWvf9WYMWO8PQ8AAADgMY++CiwoKEjXXXedt2cBAAAALotHcTtnzhy9+eabsizL2/MAAAAAHvPosYTt27dr8+bN2rBhg66//nq1atXKbfuaNWu8MhwAAADQGB7FbWhoqO6++25vzwIAAABcFo/iNjMz09tzAAAAAJfNo2duJammpkaffvqp/vznP+vUqVOSpMLCQp0+fdprwwEAAACN4dGd22PHjik+Pl4FBQVyOp2688471b59e7388styOp3KyMjw9pwAAADAJXl05/bRRx/V0KFD9eOPP6pNmzau9XfffbeysrK8NhwAAADQGB7duf3ss8/0+eefKygoyG199+7d9cMPP3hlMAAAAKCxPLpzW1dXp9ra2vPWf//992rfvv1lDwUAAAB4wqO4jY2N1RtvvOF67efnp9OnTystLa1Rf5J327ZtGjdunCIjI+Xn56f333/fbfv06dPl5+fntsTHx7vtU1ZWpilTpshmsyk0NFSJiYn8UhsAAMBVyqO4fe211/SPf/xD/fr105kzZ3T//fe7Hkl4+eWXG3ycqqoqDRo0SIsXL77gPvHx8SoqKnIt//d//+e2fcqUKTp48KA2bdqkdevWadu2bZo5c6YnpwUAAIAWzqNnbrt06aK9e/dq5cqV2rdvn06fPq3ExERNmTLF7RfMLmX06NEaPXr0RfcJDg6Ww+God9s///lPbdy4UV988YWGDh0qSXrrrbc0ZswYvfrqq4qMjGz4SQEAAKDF8yhuJSkwMFBTp0715iz12rJli8LDw3XNNdfojjvu0AsvvKAOHTpIknJychQaGuoKW0mKiYmRv7+/du7cyV9RAwAAuMp4FLfLly+/6PZp06Z5NMwvxcfHa+LEierRo4fy8/P19NNPa/To0crJyVFAQICKi4sVHh7u9p7AwECFhYWpuLj4gsd1Op1yOp2u15WVlV6ZFwAAAL7lUdw++uijbq+rq6v1008/KSgoSCEhIV6L2/vuu8/17wEDBmjgwIHq2bOntmzZolGjRnl83PT0dD333HPeGBEAAADNiEe/UPbjjz+6LadPn1ZeXp5GjBhx3i98edO1116rjh076vDhw5Ikh8Oh0tJSt31qampUVlZ2wed0JSk1NVUVFRWu5fjx41dsZgAAADQdj+K2Pr169dJLL7103l1db/r+++918uRJde7cWZIUHR2t8vJy5ebmuvbJzs5WXV2dhg0bdsHjBAcHy2azuS0AAABo+Tz+hbJ6DxYYqMLCwgbvf/r0adddWEk6cuSI9uzZo7CwMIWFhem5557TpEmT5HA4lJ+fryeffFLXXXed4uLiJEl9+/ZVfHy8HnroIWVkZKi6ulrJycm67777+KYEAACAq5BHcfvhhx+6vbYsS0VFRfrTn/6km2++ucHH+fLLLzVy5EjX65SUFElSQkKCli5dqn379undd99VeXm5IiMjFRsbq+eff17BwcGu97z33ntKTk7WqFGj5O/vr0mTJmnRokWenBYAAABaOD/LsqzGvsnf3/1pBj8/P3Xq1El33HGHXnvtNddjAy1FZWWl7Ha7KioqeEQBANAidP/9el+PgKvc0ZfGNunnNbTXPLpzW1dX5/FgAAAAwJXitV8oAwAAAHzNozu3556NbYiFCxd68hEAAABAo3kUt7t379bu3btVXV2tX//615Kkb775RgEBAfrNb37j2s/Pz887UwK46vA8IXytqZ8nBOAdHsXtuHHj1L59e7377ru65pprJP3nDzvMmDFDt9xyi+bMmePVIQEAAICG8OiZ29dee03p6emusJWka665Ri+88IJee+01rw0HAAAANIZHcVtZWakTJ06ct/7EiRM6derUZQ8FAAAAeMKjuL377rs1Y8YMrVmzRt9//72+//57/e1vf1NiYqImTpzo7RkBAACABvHomduMjAzNnTtX999/v6qrq/9zoMBAJSYm6pVXXvHqgAAAAEBDeRS3ISEhWrJkiV555RXl5+dLknr27Km2bdt6dTgAAACgMS7rjzgUFRWpqKhIvXr1Utu2beXBX/IFAAAAvMajuD158qRGjRql3r17a8yYMSoqKpIkJSYm8jVgAAAA8BmP4vbxxx9Xq1atVFBQoJCQENf6e++9Vxs3bvTacAAAAEBjePTM7d///nd98skn6tKli9v6Xr166dixY14ZDAAAAGgsj+7cVlVVud2xPaesrEzBwcGXPRQAAADgCY/i9pZbbtHy5ctdr/38/FRXV6cFCxZo5MiRXhsOAAAAaAyPHktYsGCBRo0apS+//FJnz57Vk08+qYMHD6qsrEz/+Mc/vD0jAAAA0CAe3bnt37+/vvnmG40YMULjx49XVVWVJk6cqN27d6tnz57enhEAAABokEbfua2urlZ8fLwyMjL0hz/84UrMBAAAAHik0XduW7VqpX379l2JWQAAAIDL4tFjCVOnTtXbb7/t7VkAAACAy+LRL5TV1NTonXfe0aeffqohQ4aobdu2btsXLlzoleEAAACAxmhU3H733Xfq3r27Dhw4oN/85jeSpG+++cZtHz8/P+9NBwAAADRCo+K2V69eKioq0ubNmyX958/tLlq0SBEREVdkOAAAAKAxGvXMrWVZbq83bNigqqoqrw4EAAAAeMqjXyg755exCwAAAPhSo+LWz8/vvGdqecYWAAAAzUWjnrm1LEvTp09XcHCwJOnMmTOaNWvWed+WsGbNGu9NCAAAADRQo+I2ISHB7fXUqVO9OgwAAABwORoVt5mZmVdqDgAAAOCyXdYvlAEAAADNCXELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABj+DRut23bpnHjxikyMlJ+fn56//333bZblqV58+apc+fOatOmjWJiYvTtt9+67VNWVqYpU6bIZrMpNDRUiYmJOn36dBOeBQAAAJoLn8ZtVVWVBg0apMWLF9e7fcGCBVq0aJEyMjK0c+dOtW3bVnFxcTpz5oxrnylTpujgwYPatGmT1q1bp23btmnmzJlNdQoAAABoRgJ9+eGjR4/W6NGj691mWZbeeOMNPfPMMxo/frwkafny5YqIiND777+v++67T//85z+1ceNGffHFFxo6dKgk6a233tKYMWP06quvKjIyssnOBQAAAL7XbJ+5PXLkiIqLixUTE+NaZ7fbNWzYMOXk5EiScnJyFBoa6gpbSYqJiZG/v7927tx5wWM7nU5VVla6LQAAAGj5mm3cFhcXS5IiIiLc1kdERLi2FRcXKzw83G17YGCgwsLCXPvUJz09XXa73bVERUV5eXoAAAD4QrON2yspNTVVFRUVruX48eO+HgkAAABe0Gzj1uFwSJJKSkrc1peUlLi2ORwOlZaWum2vqalRWVmZa5/6BAcHy2azuS0AAABo+Zpt3Pbo0UMOh0NZWVmudZWVldq5c6eio6MlSdHR0SovL1dubq5rn+zsbNXV1WnYsGFNPjMAAAB8y6fflnD69GkdPnzY9frIkSPas2ePwsLC1LVrVz322GN64YUX1KtXL/Xo0UP/8z//o8jISE2YMEGS1LdvX8XHx+uhhx5SRkaGqqurlZycrPvuu49vSgAAALgK+TRuv/zyS40cOdL1OiUlRZKUkJCgZcuW6cknn1RVVZVmzpyp8vJyjRgxQhs3blTr1q1d73nvvfeUnJysUaNGyd/fX5MmTdKiRYua/FwAAADge36WZVm+HsLXKisrZbfbVVFRwfO3QDPR/ffrfT0CrnJHXxrr6xEuimsEvtbU10hDe63ZPnMLAAAANBZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwRqCvB7hadf/9el+PgKvc0ZfG+noEAAC8jju3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwRrOO22effVZ+fn5uS58+fVzbz5w5o6SkJHXo0EHt2rXTpEmTVFJS4sOJAQAA4EvNOm4l6frrr1dRUZFr2b59u2vb448/ro8++kirV6/W1q1bVVhYqIkTJ/pwWgAAAPhSs/+e28DAQDkcjvPWV1RU6O2339aKFSt0xx13SJIyMzPVt29f7dixQ8OHD2/qUQEAAOBjzf7O7bfffqvIyEhde+21mjJligoKCiRJubm5qq6uVkxMjGvfPn36qGvXrsrJybnoMZ1OpyorK90WAAAAtHzNOm6HDRumZcuWaePGjVq6dKmOHDmiW265RadOnVJxcbGCgoIUGhrq9p6IiAgVFxdf9Ljp6emy2+2uJSoq6gqeBQAAAJpKs34sYfTo0a5/Dxw4UMOGDVO3bt3017/+VW3atPH4uKmpqUpJSXG9rqysJHABAAAM0Kzv3P5SaGioevfurcOHD8vhcOjs2bMqLy9326ekpKTeZ3R/Ljg4WDabzW0BAABAy9ei4vb06dPKz89X586dNWTIELVq1UpZWVmu7Xl5eSooKFB0dLQPpwQAAICvNOvHEubOnatx48apW7duKiwsVFpamgICAjR58mTZ7XYlJiYqJSVFYWFhstlseuSRRxQdHc03JQAAAFylmnXcfv/995o8ebJOnjypTp06acSIEdqxY4c6deokSXr99dfl7++vSZMmyel0Ki4uTkuWLPHx1AAAAPCVZh23K1euvOj21q1ba/HixVq8eHETTQQAAIDmrEU9cwsAAABcDHELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADAGcQsAAABjELcAAAAwBnELAAAAYxC3AAAAMAZxCwAAAGMQtwAAADCGMXG7ePFide/eXa1bt9awYcO0a9cuX48EAACAJmZE3K5atUopKSlKS0vTV199pUGDBikuLk6lpaW+Hg0AAABNyIi4XbhwoR566CHNmDFD/fr1U0ZGhkJCQvTOO+/4ejQAAAA0oRYft2fPnlVubq5iYmJc6/z9/RUTE6OcnBwfTgYAAICmFujrAS7Xv/71L9XW1ioiIsJtfUREhL7++ut63+N0OuV0Ol2vKyoqJEmVlZVXbtBfqHP+1GSfBdSnKf/z7gmuEfga1whwcU19jZz7PMuyLrpfi49bT6Snp+u55547b31UVJQPpgF8w/6GrycAmjeuEeDifHWNnDp1Sna7/YLbW3zcduzYUQEBASopKXFbX1JSIofDUe97UlNTlZKS4npdV1ensrIydejQQX5+fld0Xly+yspKRUVF6fjx47LZbL4eB2iWuE6Ai+MaaXksy9KpU6cUGRl50f1afNwGBQVpyJAhysrK0oQJEyT9J1azsrKUnJxc73uCg4MVHBzsti40NPQKTwpvs9ls/BcScAlcJ8DFcY20LBe7Y3tOi49bSUpJSVFCQoKGDh2qm266SW+88Yaqqqo0Y8YMX48GAACAJmRE3N577706ceKE5s2bp+LiYg0ePFgbN24875fMAAAAYDYj4laSkpOTL/gYAswSHBystLS08x4tAfD/cZ0AF8c1Yi4/61LfpwAAAAC0EC3+jzgAAAAA5xC3AAAAMAZxCwAAAGMQt/C6kydPKjw8XEePHvX1KB7JyMjQuHHjfD0GDNbSr5GNGzdq8ODBqqur8/UoMBjXCTxF3MLrXnzxRY0fP17du3eXJO3du1eTJ09WVFSU2rRpo759++rNN9/06NiLFy9W9+7d1bp1aw0bNky7du1q9DEKCgo0duxYhYSEKDw8XE888YRqampc2x988EF99dVX+uyzzzyaEbiUX14jJ0+eVHx8vCIjIxUcHKyoqCglJyc3+u+2b9u2TePGjVNkZKT8/Pz0/vvvezRfWVmZpkyZIpvNptDQUCUmJur06dOu7fHx8WrVqpXee+89j44PNMQvr5OfO3nypLp06SI/Pz+Vl5c36rjp6em68cYb1b59e4WHh2vChAnKy8tr9Hy/+93vNGTIEAUHB2vw4MHnbec68R3iFl71008/6e2331ZiYqJrXW5ursLDw/WXv/xFBw8e1B/+8AelpqbqT3/6U6OOvWrVKqWkpCgtLU1fffWVBg0apLi4OJWWljb4GLW1tRo7dqzOnj2rzz//XO+++66WLVumefPmufYJCgrS/fffr0WLFjVqPqAh6rtG/P39NX78eH344Yf65ptvtGzZMn366aeaNWtWo45dVVWlQYMGafHixZc145QpU3Tw4EFt2rRJ69at07Zt2zRz5ky3faZPn841giumvuvk5xITEzVw4ECPjr1161YlJSVpx44d2rRpk6qrqxUbG6uqqqpGH+vBBx/Uvffee8HtXCc+YgFetHr1aqtTp06X3O+3v/2tNXLkyEYd+6abbrKSkpJcr2tra63IyEgrPT29wcf4+OOPLX9/f6u4uNi1bunSpZbNZrOcTqdr3datW62goCDrp59+atSMwKU09Bp58803rS5dunj8OZKstWvXNvp9hw4dsiRZX3zxhWvdhg0bLD8/P+uHH35wrTt27JglyTp8+LDHMwIXcrHrZMmSJdZtt91mZWVlWZKsH3/88bI+q7S01JJkbd261aP3p6WlWYMGDap3G9eJb3DnFl712WefaciQIZfcr6KiQmFhYQ0+7tmzZ5Wbm6uYmBjXOn9/f8XExCgnJ6fBx8nJydGAAQPc/npdXFycKisrdfDgQde6oUOHqqamRjt37mzwsYGGaMg1UlhYqDVr1ui2225roqn+v5ycHIWGhmro0KGudTExMfL393e7Hrp27aqIiAge38EVcaHr5NChQ5o/f76WL18uf3/vJExFRYUkNepnUkNxnfgGcQuvOnbsmCIjIy+6z+eff65Vq1ad939zXsy//vUv1dbWnvcnlSMiIlRcXNzg4xQXF9d7jHPbzgkJCZHdbtexY8cafGygIS52jUyePFkhISH61a9+JZvNpv/93/9t4un+cx2Eh4e7rQsMDFRYWNh511pkZCTXCK6I+q4Tp9OpyZMn65VXXlHXrl298jl1dXV67LHHdPPNN6t///5eOeYvcZ00PeIWXvXvf/9brVu3vuD2AwcOaPz48UpLS1NsbGwTTtZ4bdq00U8//eTrMWCYi10jr7/+ur766it98MEHys/PV0pKShNP1zhcI7hS6rtOUlNT1bdvX02dOtVrn5OUlKQDBw5o5cqVXjvmL3GdND3iFl7VsWNH/fjjj/VuO3TokEaNGqWZM2fqmWeeafRxAwICVFJS4ra+pKREDoejwcdxOBz1HuPctp8rKytTp06dGjUncCkXu0YcDof69Omj//7v/9af//xnLV26VEVFRU06n8PhOO+XNGtqalRWVsY1giZT33WSnZ2t1atXKzAwUIGBgRo1apRr37S0tEZ/RnJystatW6fNmzerS5cuXpm7PlwnTY+4hVfdcMMNOnTo0HnrDx48qJEjRyohIUEvvvhio48bFBSkIUOGKCsry7Wurq5OWVlZio6ObvBxoqOjtX//frcf3ps2bZLNZlO/fv1c6/Lz83XmzBndcMMNjZ4VuJgLXSO/dO67MZ1O55UeyU10dLTKy8uVm5vrWpedna26ujoNGzbMte7MmTPKz8/nGsEVUd918re//U179+7Vnj17tGfPHtdjO5999pmSkpIafGzLspScnKy1a9cqOztbPXr08OrsP8d14iO+/o02mGXfvn1WYGCgVVZW5lq3f/9+q1OnTtbUqVOtoqIi11JaWtqoY69cudIKDg62li1bZh06dMiaOXOmFRoa6vbNB5dSU1Nj9e/f34qNjbX27Nljbdy40erUqZOVmprqtl9mZqZ17bXXNmo+oCHqu0bWr19vvfPOO9b+/futI0eOWOvWrbP69u1r3XzzzY069qlTp6zdu3dbu3fvtiRZCxcutHbv3m0dO3asUceJj4+3brjhBmvnzp3W9u3brV69elmTJ09222fz5s1Wu3btrKqqqkYdG2iI+q6TX9q8ebNH35Ywe/Zsy263W1u2bHH7mdTYb8f59ttvrd27d1sPP/yw1bt3b9e19/Nv3uE68Q3iFl530003WRkZGa7XaWlplqTzlm7durn2OXLkiCXJ2rx580WP/dZbb1ldu3a1goKCrJtuusnasWOH2/aEhATrtttuu+gxjh49ao0ePdpq06aN1bFjR2vOnDlWdXW12z6xsbGN+ooxoDF+eY1kZ2db0dHRlt1ut1q3bm316tXLeuqpp9x+aDfkGjn3w/6XS0JCgmuftLQ0t2uvPidPnrQmT55stWvXzrLZbNaMGTOsU6dOue0zc+ZM6+GHH27MaQON8svr5Jfqi9uGXCf1XSOSrMzMTNc+DflZctttt9V7nCNHjrj24TrxDeIWXnfurlNtbW2D35OdnW2FhoZe9H+lN8Stt95qpaWlXdYxDhw4YIWHh1vl5eWXdRzgQnx5jUybNs0tdj1x4sQJKywszPruu+8u6zjAxbT0nyVcJ74T2ERPP+AqMnbsWH377bf64YcfFBUV1aD3fPzxx3r66ad1zTXXePy5FRUVys/P1/r16z0+hiQVFRVp+fLlstvtl3Uc4EJ8dY1YlqUtW7Zo+/btHh9Dko4ePaolS5Zc0WcVgZb+s4TrxHf8LMuyfD0EAAAA4A18WwIAAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AJAC/fss89q8ODBjXrP7bffrscee+yKzAMAvkTcAkAz4ufnd9Hl2WefPe89c+fOVVZWVtMPCwDNEH/EAQCakaKiIte/V61apXnz5ikvL8+1rl27dq5/W5al2tpatWvXzm09AFzNuHMLAM2Iw+FwLXa7XX5+fq7XX3/9tdq3b68NGzZoyJAhCg4O1vbt2897LGH69OmaMGGCnnvuOXXq1Ek2m02zZs3S2bNnL/i5TqdTc+fO1a9+9Su1bdtWw4YN05YtW678CQOAl3HnFgBamN///vd69dVXde211+qaa66pN0KzsrLUunVrbdmyRUePHtWMGTPUoUMHvfjii/UeMzk5WYcOHdLKlSsVGRmptWvXKj4+Xvv371evXr2u8BkBgPdw5xYAWpj58+frzjvvVM+ePRUWFlbvPkFBQXrnnXd0/fXXa+zYsZo/f74WLVqkurq68/YtKChQZmamVq9erVtuuUU9e/bU3LlzNWLECGVmZl7p0wEAr+LOLQC0MEOHDr3kPoMGDVJISIjrdXR0tE6fPq3jx4+rW7dubvvu379ftbW16t27t9t6p9OpDh06eGdoAGgixC0AtDBt27b16vFOnz6tgIAA5ebmKiAgwG0bv6gGoKUhbgHAQHv37tW///1vtWnTRpK0Y8cOtWvXTlFRUefte8MNN6i2tlalpaW65ZZbmnpUAPAqnrkFAAOdPXtWiYmJOnTokD7++GOlpaUpOTlZ/v7n/9d+7969NWXKFE2bNk1r1qzRkSNHtGvXLqWnp2v9+vU+mB4APMedWwAw0KhRo9SrVy/deuutcjqdmjx5cr1/AOKczMxMvfDCC5ozZ45++OEHdezYUcOHD9ddd93VdEMDgBf4WZZl+XoIAID3TJ8+XeXl5Xr//fd9PQoANDkeSwAAAIAxiFsAAAAYg8cSAAAAYAzu3AIAAMAYxC0AAACMQdwCAADAGMQtAAAAjEHcAgAAwBjELQAAAIxB3AIAAMAYxC0AAACMQdwCAADAGP8PnobgY4f3tngAAAAASUVORK5CYII=",
      "text/plain": [
       "<Figure size 800x600 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "xticks = []\n",
    "y = []\n",
    "x = range(len(jklDist))\n",
    "for triple, frequency in sorted(jklDist.items()):\n",
    "    xticks.append(str(triple))\n",
    "    y.append(frequency)\n",
    "plt.figure(figsize=(8, 6))\n",
    "plt.xticks(x, labels=xticks)\n",
    "plt.xlabel(\"Triple\")\n",
    "plt.ylabel(\"Frequency\")\n",
    "plt.bar(x, y)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are in line with expectations from [literature](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.47.3975). The script finds that the (4,2,1) triple has the highest frequency, which correctly identifies the fcc Pt lattice that the particle was cut from. The (3,1,0) triple, which is not present in perfect bulk fcc Pt, is found here as well and corresponds to the undercoordinated facet and edge sites. The (2,0,0) triple characterizes the vertices (tips) of the octahedron."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "2fef78300edb407ce1be44772deb17a778289952a424405cf808fe8157c8b8f2"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
