# In Silico Characterization of Nanoparticles

## Description

Read the book: https://bjk24.gitlab.io/in-silico-review

This book supplements the review article [In
Silico Characterization of Nanoparticles](https://doi.org/10.48550/arXiv.2303.03746).

```
Björn Kirchhoff, Ph.D.
Institute of Electrochemistry
Ulm University
Albert-Einstein-Allee 47
89134 Ulm, Germany
bjoern.kirchhoff [at] protonmail [dot] com
```

## Installation and usage

In order to execute this book, download the "octahdron-111-3nm.zip" data set available
[via Zenodo](https://doi.org/10.5281/zenodo.6322004).
For more details, please refer to the
[Setup](https://bjk24.gitlab.io/in-silico-review/docs/setup.html)
chapter of the book.

## Authors and acknowledgment

Björn Kirchhoff - main author and maintainer.<br/>
Julian Bessner - contributed parts of the Common Neighbor Analysis.

## License

This repository is provided under the Creative Commons Attribution 4.0
International license. Please cite the following DOI when referencing
this repository: [10.5281/zenodo.7712649](https://doi.org/10.5281/zenodo.7712649).